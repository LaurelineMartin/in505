//Exo2 - TD1
#include <iostream>
#include <cstring>

void echange(int &a, int &b){
	int temp;
	temp = a;
	a = b;
	b = temp;}
	
int main(){
	int nb1 = 6;
	int nb2 = 7;
	std::cout << "Avant : Nb1 = " << nb1 << "  Nb2 = " << nb2 << '\n';
	echange(nb1, nb2);
	std::cout << "Apres : Nb1 = " << nb1 << "  Nb2 = " << nb2 << '\n';
	
}

