////Exo 3 - TD1

#include <iostream>
#include <cstring>

void tab_lecture(const int* tab, int taille){
	int i; 
	std::cout << std::endl;
	for(i = 0; i < taille; i++){
		std::cout << tab[i] << " ";}
	std::cout << std::endl;}

void tab_ecriture(int tab[], int taille){
	for(int i =0; i < taille; i++){
		tab[i] = i;}}

int main(){
	int taille = 8;
	int tab[12];
	
	tab_ecriture(tab, taille);
	tab_lecture(tab, taille);
}
