#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class CString{
	private:
		char* mot;
		static int compte;
	public:
		CString(char* m){
			mot = new char[(strlen(m)+1)];
			strcpy(mot, m);
			++compte;}
		CString(char c){
			mot = new char[2];
			mot[0] = c;
			++compte;}
		CString(){
			mot = new char[1];
			mot[0] = '\0';
			++compte;}
		
		static int nbrChaines(){
			return CString::compte;}
		
		CString plus(char* c){
			char temp[20];
			strcpy(temp, mot);
			strcat(temp, c);
			return temp;}
		
		char* getString(){
			return mot;}
		
		bool plusGrandQue(CString s){
			//~ for(int i = 0; i < strlen(mot) || strlen(s.mot); i++){
				//~ if(mot[i] > s.mot[i])
				if(strlen(mot) > strlen(s.mot)){
					return true;}
			return false;}
		
		bool infOuEgale(CString s){
			if(!CString::plusGrandQue(s)){
				return true;}
			return false;}
		
		CString grand(CString s){
			if(CString::plusGrandQue(s)){
				return CString::mot;}
			return s;}};

int CString::compte = 0;

int main()
{
	CString s1("toto"), s2('q'), s3 ;
	cout << "Nombre de chaines " << CString::nbrChaines() << endl ;
//~ //afficher le nombre de chaines créées
	cout << "s3 = " << s3.getString() << endl ;
	s3 = s1.plus("c") ;
	cout << "s3 = " << s3.getString() << endl ;
	if( s1.plusGrandQue(s2) ) // si s1 > s2 au sens alphabétique
		cout << s1.getString() <<  " est plus grand que " << s2.getString() << endl ;
	if( s1.infOuEgale(s2) ) // si s1 <= s2 au sens alphabétique
		cout << s1.getString() <<  " estplus petit que " << s2.getString() << endl ;
	s3 = s1.grand(s2); 
	cout << "Nouveau s3 : " << s3.getString() << endl;

}







