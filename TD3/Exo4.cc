//Exo4- TD1

#include <iostream>
#include <cstring>

class Point{
	public :
		int x, y;
		Point(){
			x = 0;
			y = 0;}
		Point(int xx, int yy){
			x = xx;
			y = yy;}
		Point (const Point &p){
			x = p.x;
			y = p.y;}
	
		void afficher(){
			std::cout << x << std::endl;
			std::cout << y << std::endl;}
	
		void cloner (const Point &p){
			x=p.x;
			y = p.y;}
		
		~Point(){
	std::cout << "appel destructeur" << std::endl;}};
	

int main(){
	Point p = Point(12, 24);
	p.afficher();
	}
