#ifndef __POINTCOLOR_HH
#define __POINTCOLOR_HH

#include <iostream>
#include <stdlib.h>
#include <cstring>

class Point{
	public :
		int x, y;
		Point();
		Point(int x, int y);
		Point(const Point &p);
		void afficher();
		void cloner(const Point &p);
		~Point();};


class PointColor : public Point{
	public :
		std::string color;
		PointColor();
		PointColor(int xx, int yy, std::string col);
		PointColor (const PointColor &p);
		void affColor();
		void cloneColor(const PointColor &p);
		~PointColor();};


#endif
